clean:
	rm -rf roles

roles:
	ansible-galaxy install -r requirements.yml -p roles

silica: roles
	ansible-playbook -i inventory.ini --vault-password-file .vault_pass --extra-vars "variable_host=silica" --extra-vars="@kubernetes.yml" -u root playbook.yml

asuka: roles
	ansible-playbook -i inventory.ini --vault-password-file .vault_pass --extra-vars "variable_host=asuka" --extra-vars="@kubernetes.yml" -u root playbook.yml

astolfo: roles
	ansible-playbook -i inventory.ini --vault-password-file .vault_pass --extra-vars "variable_host=astolfo" --extra-vars="@kubernetes.yml" -u root playbook.yml

runonip: roles
	ansible-playbook -i $(ip), --vault-password-file .vault_pass --extra-vars "variable_host=$(ip)" --extra-vars="@kubernetes.yml" -u root playbook.yml

nfs: roles
	ansible-playbook -i inventory.ini --vault-password-file .vault_pass --extra-vars "variable_host=$(host)" --extra-vars="@kubernetes.yml" -u root play-nfs.yml

edit:
	ansible-vault edit kubernetes.yml --vault-password-file .vault_pass